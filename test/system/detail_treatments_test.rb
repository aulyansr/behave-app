require "application_system_test_case"

class DetailTreatmentsTest < ApplicationSystemTestCase
  setup do
    @detail_treatment = detail_treatments(:one)
  end

  test "visiting the index" do
    visit detail_treatments_url
    assert_selector "h1", text: "Detail Treatments"
  end

  test "creating a Detail treatment" do
    visit detail_treatments_url
    click_on "New Detail Treatment"

    fill_in "Content", with: @detail_treatment.content
    fill_in "Title", with: @detail_treatment.title
    fill_in "Treatment", with: @detail_treatment.treatment_id
    click_on "Create Detail treatment"

    assert_text "Detail treatment was successfully created"
    click_on "Back"
  end

  test "updating a Detail treatment" do
    visit detail_treatments_url
    click_on "Edit", match: :first

    fill_in "Content", with: @detail_treatment.content
    fill_in "Title", with: @detail_treatment.title
    fill_in "Treatment", with: @detail_treatment.treatment_id
    click_on "Update Detail treatment"

    assert_text "Detail treatment was successfully updated"
    click_on "Back"
  end

  test "destroying a Detail treatment" do
    visit detail_treatments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Detail treatment was successfully destroyed"
  end
end
