require 'test_helper'

class DetailTreatmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @detail_treatment = detail_treatments(:one)
  end

  test "should get index" do
    get detail_treatments_url
    assert_response :success
  end

  test "should get new" do
    get new_detail_treatment_url
    assert_response :success
  end

  test "should create detail_treatment" do
    assert_difference('DetailTreatment.count') do
      post detail_treatments_url, params: { detail_treatment: { content: @detail_treatment.content, title: @detail_treatment.title, treatment_id: @detail_treatment.treatment_id } }
    end

    assert_redirected_to detail_treatment_url(DetailTreatment.last)
  end

  test "should show detail_treatment" do
    get detail_treatment_url(@detail_treatment)
    assert_response :success
  end

  test "should get edit" do
    get edit_detail_treatment_url(@detail_treatment)
    assert_response :success
  end

  test "should update detail_treatment" do
    patch detail_treatment_url(@detail_treatment), params: { detail_treatment: { content: @detail_treatment.content, title: @detail_treatment.title, treatment_id: @detail_treatment.treatment_id } }
    assert_redirected_to detail_treatment_url(@detail_treatment)
  end

  test "should destroy detail_treatment" do
    assert_difference('DetailTreatment.count', -1) do
      delete detail_treatment_url(@detail_treatment)
    end

    assert_redirected_to detail_treatments_url
  end
end
