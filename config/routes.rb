Rails.application.routes.draw do
  resources :doctors
  resources :detail_treatments
  resources :treatments
  resources :sliders
  resources :videos
  resources :events
  mount Ckeditor::Engine => '/ckeditor'
  resources :writers
  resources :articles
  resources :categories
  root 'pages#home'
  resources :appointments
  resources :contacts
  get 'app' => "app#index"
  get 'invoice' => "app#invoice"
  get 'pemeriksaan' => "app#pemeriksaan"
  get 'report' => "app#report"
  get 'app-dokter' => "app#app_dokter"
  get 'contact-us', to: 'pages#contact', as: :contact_us
  get 'create-appointment', to: 'pages#create_appointment', as: :create_appointment
  get 'blog', to: 'pages#blog', as: :list_blog
  resources :users
  resources :setups
  devise_for :users,
  path:'user',
  path_names: {
    sign_in: 'login',
    sign_out: 'logout'
  }
  get 'list-page', to: 'pages#index', as: :list_pages
  resources :pages, path: ''
  get '/blog/:id', to: 'articles#show', as: :blog_detail
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
