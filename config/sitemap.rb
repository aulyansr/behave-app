# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://behave.rehab/"
SitemapGenerator::Sitemap.ping_search_engines

SitemapGenerator::Sitemap.create do
  add root_path, :changefreq => 'daily'
  add contact_us_path, :changefreq => 'daily'
  add create_appointment_path, :changefreq => 'daily'

  Page.all.each do |x|
  add  page_path(x.slug), :changefreq => 'monthly', :lastmod => x.updated_at
  end
end
