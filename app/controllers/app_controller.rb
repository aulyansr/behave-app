class AppController < ApplicationController
  before_action :authenticate_user!
  layout 'admin'
  def index
    @articles = Article.last(5)
    @events = Event.last(5)
    @appointment = Appointment.all
    @message = Contact.all
  end
  def invoice
    #code
  end
  def app_dokter
    #code
  end
end
