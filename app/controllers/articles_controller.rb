class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show]
  layout 'admin', except: [:show]
  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    @last_event = Event.last
    @related = Article.all.where.not(id: @article.id).where(category_id:  @article.category_id).limit(5)
    prepare_meta_tags(
    title: @article.title,
    description: @article.meta_description,
    image: @article.image,
    twitter: {
      site_name: "BEHAVE CLINIC | #{@article.title || @article.title}" ,
      site: '@behave_clinic',
      creator: '@behave_clinic',
      card: 'summary',
      description:   @article.meta_description || ActionView::Base.full_sanitizer.sanitize(@article.content).truncate(250),
      title: @article.title || @article.title,
      image: request.protocol + request.host_with_port + @article.image.url
    },
    og: {
      url: request.protocol + request.host_with_port+"/blog/"+ @article.slug,
      site_name:  "BEHAVE CLINIC | #{@article.title || @article.title}" ,
      title: @article.title || @article.title,
      image: request.protocol + request.host_with_port + @article.image.url,
      type: 'article',
      description:  @article.meta_description || ActionView::Base.full_sanitizer.sanitize(@article.content).truncate(250)
      })
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :content, :category_id, :writer_id, :slug, :image, :meta_description)
    end
end
