class DetailTreatmentsController < ApplicationController
  before_action :set_detail_treatment, only: [:show, :edit, :update, :destroy]

  # GET /detail_treatments
  # GET /detail_treatments.json
  def index
    @detail_treatments = DetailTreatment.all
  end

  # GET /detail_treatments/1
  # GET /detail_treatments/1.json
  def show
  end

  # GET /detail_treatments/new
  def new
    @detail_treatment = DetailTreatment.new
  end

  # GET /detail_treatments/1/edit
  def edit
  end

  # POST /detail_treatments
  # POST /detail_treatments.json
  def create
    @detail_treatment = DetailTreatment.new(detail_treatment_params)

    respond_to do |format|
      if @detail_treatment.save
        format.html { redirect_to @detail_treatment, notice: 'Detail treatment was successfully created.' }
        format.json { render :show, status: :created, location: @detail_treatment }
      else
        format.html { render :new }
        format.json { render json: @detail_treatment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /detail_treatments/1
  # PATCH/PUT /detail_treatments/1.json
  def update
    respond_to do |format|
      if @detail_treatment.update(detail_treatment_params)
        format.html { redirect_to @detail_treatment, notice: 'Detail treatment was successfully updated.' }
        format.json { render :show, status: :ok, location: @detail_treatment }
      else
        format.html { render :edit }
        format.json { render json: @detail_treatment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /detail_treatments/1
  # DELETE /detail_treatments/1.json
  def destroy
    @detail_treatment.destroy
    respond_to do |format|
      format.html { redirect_to detail_treatments_url, notice: 'Detail treatment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_detail_treatment
      @detail_treatment = DetailTreatment.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def detail_treatment_params
      params.require(:detail_treatment).permit(:title, :content, :treatment_id)
    end
end
