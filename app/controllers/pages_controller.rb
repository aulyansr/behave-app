class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:index, :edit, :new]
  layout 'admin', only: [:index, :edit, :new]
  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.where(slug: ['about-us','homepage'])
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    prepare_meta_tags(
      title: @page.title , keywords: @page.focus_keyword, description: @page.meta_description, twitter:{description:@page.meta_description}, og:{description:@page.meta_description}
    )
  end

  # GET /pages/new
  def new
    @page = Page.new
  end

  # GET /pages/1/edit
  def edit
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)

    respond_to do |format|
      if @page.save
        format.html { redirect_to pages_path, notice: 'Page was successfully created.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to pages_path, notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def home
    @video = Video.all.order(created_at: :desc)
    @last_event = Event.last
    @page = Page.find("homepage")
  end

  def contact
    prepare_meta_tags(
      title: "Contact Behave Clinic" ,
      description: "Get a recovery experience from experts with eastern culture full of love and friendliness",
      twitter:{description:"Kindly ensure you provide us with your contact details, so we can reach you for further questions or clarifications and/or to schedule further activities. Our consultants will be in touch with you as soon as possible. "},
      og:{description:"Kindly ensure you provide us with your contact details, so we can reach you for further questions or clarifications and/or to schedule further activities. Our consultants will be in touch with you as soon as possible. "}
    )
   @contact = Contact.new
  end

  def create_appointment
   @appointment = Appointment.new
      @contact = Contact.new
  end

  def blog
    @last_event = Event.last
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:title, :slug, :content, :seo_title, :meta_description, :vision_and_mission, :our_story, :cover_image, :focus_keyword, sliders_attributes: [:id, :image, :caption, :_destroy, :page_id])
    end
end
