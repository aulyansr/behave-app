class ApplicationController < ActionController::Base
  before_action :prepare_meta_tags
  layout :layout_by_resource
  def layout_by_resource
    if devise_controller?
      "login"
    else
      "application"
    end
  end
  def after_sign_in_path_for(resource_or_scope)
      app_path
  end

  private
  def prepare_meta_tags(options={})
    site_name   = 'Behave Clinic'
    title       = 'There is No Health Without Mental Health'
    description = 'Behave merupakan pusat pemulihan (recovery center) eksklusif yang berfokus pada kesehatan jiwa bagi dewasa, tumbuh kembang anak dan remaja, juga pemulihan pecandu narkoba yang ditangani oleh profesional yang memahami kebutuhan Anda.'
    keywords    = 'rehab'
    image       = 'http://behave.rehab/assets/logo.png'
    current_url = "http://behave.rehab"

    # Let's prepare a nice set of defaults
    defaults = {
      site:        site_name,
      title:       title,
      image:       image,
      description: description,
      keywords:    keywords,
      reverse: true,
      twitter: {
        site_name: site_name,
        site: '@behaveclinic',
        creator: '@behaveclinic',
        card: 'summary_large_image',
        description: description,
        title: title,
        image: image
      },
      og: {
        url: current_url,
        site_name: site_name,
        title: title,
        image: image,
        description: description,
        type: 'website'
      }
    }

    options.reverse_merge!(defaults)

    set_meta_tags options
  end
end
