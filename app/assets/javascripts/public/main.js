$( document ).ready(function() {

$('.home-slider').owlCarousel({
  autoplayHoverPause:false,
  autoplayTimeout:6000,
  loop:true,
  dots: false,

  responsive:{
    0:{
      autoWidth:false,
      mouseDrag: false,
      center:false,
      items:1
    },
    600:{
      nav:true,
      items:3
    },
    1000:{
      autoWidth:true,
      center:true,
      nav:true,
      items:3
    }
  }

})
$(function () {
  $(document).scroll(function () {
    var $nav = $("#main-nav");
    $nav.toggleClass('scrolled shadow', $(this).scrollTop() > $nav.height());
  });

   $(".video-0,.video-1, .video-2, .video-3, .video-4, .video-5, .video-6, .video-7, .video-8").on("click", function(event) {
     event.preventDefault();
     $(".video_case iframe").prop("src", $(event.currentTarget).attr("href"));
   });
});


$('.x-card-title').matchHeight();

 });
