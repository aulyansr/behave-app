module ApplicationHelper

  def count_row(x)
    if x == 2
      "6"
    elsif x >= 4
      "4"
    else
      "12"
    end
  end
end
