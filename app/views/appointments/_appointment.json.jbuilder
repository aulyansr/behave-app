json.extract! appointment, :id, :fullname, :email, :phonenumber, :arrival_date, :created_at, :updated_at
json.url appointment_url(appointment, format: :json)
