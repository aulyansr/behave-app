json.extract! event, :id, :title, :meta_description, :image, :content, :start_date, :start_time, :end_date, :end_time, :venue_name, :street, :city, :zip_code, :created_at, :updated_at
json.url event_url(event, format: :json)
