json.extract! video, :id, :youtube_url, :title, :image, :created_at, :updated_at
json.url video_url(video, format: :json)
