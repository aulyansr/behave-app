json.extract! doctor, :id, :title, :description, :image, :slug, :created_at, :updated_at
json.url doctor_url(doctor, format: :json)
