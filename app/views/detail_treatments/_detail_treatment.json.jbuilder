json.extract! detail_treatment, :id, :title, :content, :treatment_id, :created_at, :updated_at
json.url detail_treatment_url(detail_treatment, format: :json)
