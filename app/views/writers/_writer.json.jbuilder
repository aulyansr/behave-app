json.extract! writer, :id, :title, :image, :description, :created_at, :updated_at
json.url writer_url(writer, format: :json)
