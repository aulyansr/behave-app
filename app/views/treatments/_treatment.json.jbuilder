json.extract! treatment, :id, :title, :cover_image, :image, :content, :meta_description, :created_at, :updated_at
json.url treatment_url(treatment, format: :json)
