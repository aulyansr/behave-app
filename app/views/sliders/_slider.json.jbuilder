json.extract! slider, :id, :image, :title, :page_id, :caption, :created_at, :updated_at
json.url slider_url(slider, format: :json)
