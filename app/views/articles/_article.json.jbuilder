json.extract! article, :id, :title, :content, :category_id, :writer_id, :slug, :image, :created_at, :updated_at
json.url article_url(article, format: :json)
