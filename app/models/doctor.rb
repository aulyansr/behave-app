class Doctor < ApplicationRecord
  extend FriendlyId
  friendly_id :title
  mount_uploader :image, ImageUploader

end
