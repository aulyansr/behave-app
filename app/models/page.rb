class Page < ApplicationRecord
  extend FriendlyId
  friendly_id :title

  validates :title, presence: true

  mount_uploader :cover_image, ImageUploader

  has_many :sliders, inverse_of: :page
  accepts_nested_attributes_for :sliders, reject_if: :all_blank, allow_destroy: true

end
