class Treatment < ApplicationRecord
    extend FriendlyId
    friendly_id :title

    mount_uploader :image, ImageUploader
    mount_uploader :cover_image, ImageUploader

    has_many :sliders, inverse_of: :treatment
    accepts_nested_attributes_for :sliders, reject_if: :all_blank, allow_destroy: true
    has_many :detail_treatments, inverse_of: :treatment
    accepts_nested_attributes_for :detail_treatments, reject_if: :all_blank, allow_destroy: true

end
