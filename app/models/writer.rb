class Writer < ApplicationRecord
  extend FriendlyId
  friendly_id :title
end
