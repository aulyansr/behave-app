class Article < ApplicationRecord
  extend FriendlyId
  friendly_id :title

  mount_uploader :image, ImageUploader

  belongs_to :category
  belongs_to :writer
end
