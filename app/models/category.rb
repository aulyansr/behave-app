class Category < ApplicationRecord
  extend FriendlyId
  friendly_id :title
  
  has_many :articles
end
