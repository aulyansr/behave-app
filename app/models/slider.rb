class Slider < ApplicationRecord
  mount_uploader :image, ImageUploader
  belongs_to :page, optional:true
  belongs_to :treatment, optional:true
end
