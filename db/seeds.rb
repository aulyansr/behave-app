# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
u = User.new
u.username = "administration"
u.role = "root"
u.email = "admin@phoenix.co.id"
u.password = "meowmeow"
u.save

s = Setup.new
s.id = 1
s.site_name = 'CMS'
s.site_title = 'CMS'
s.site_url = 'http://localhost:3000/'
s.save
