class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title
      t.string :meta_description
      t.string :image
      t.text :content
      t.date :start_date
      t.time :start_time
      t.date :end_date
      t.time :end_time
      t.string :venue_name
      t.string :street
      t.string :city
      t.string :zip_code

      t.timestamps
    end
  end
end
