class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :content
      t.references :category, foreign_key: true
      t.references :writer, foreign_key: true
      t.string :slug
      t.string :image

      t.timestamps
    end
  end
end
