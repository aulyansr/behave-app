class AddMapToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :map, :text
  end
end
