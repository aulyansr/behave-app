class CreateAppointments < ActiveRecord::Migration[5.2]
  def change
    create_table :appointments do |t|
      t.string :fullname
      t.string :email
      t.string :phonenumber
      t.string :arrival_date

      t.timestamps
    end
  end
end
