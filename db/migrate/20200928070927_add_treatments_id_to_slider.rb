class AddTreatmentsIdToSlider < ActiveRecord::Migration[5.2]
  def change
    add_column :sliders, :treatment_id, :string
  end
end
