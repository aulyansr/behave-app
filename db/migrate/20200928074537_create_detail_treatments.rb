class CreateDetailTreatments < ActiveRecord::Migration[5.2]
  def change
    create_table :detail_treatments do |t|
      t.string :title
      t.text :content
      t.references :treatment, foreign_key: true

      t.timestamps
    end
  end
end
