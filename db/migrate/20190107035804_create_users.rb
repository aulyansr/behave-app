class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username
      t.string :name
      t.string :slug
      t.string :role
      t.string :gender
      t.text :bio
      t.string :avatar

      t.timestamps
    end
    add_index :users, :slug, unique: true
  end
end
