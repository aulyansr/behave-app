class AddSlugToWriter < ActiveRecord::Migration[5.2]
  def change
    add_column :writers, :slug, :string
  end
end
