class CreateTreatments < ActiveRecord::Migration[5.2]
  def change
    create_table :treatments do |t|
      t.string :title
      t.string :cover_image
      t.string :image
      t.text :content
      t.text :meta_description

      t.timestamps
    end
  end
end
