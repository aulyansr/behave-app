class CreateWriters < ActiveRecord::Migration[5.2]
  def change
    create_table :writers do |t|
      t.string :title
      t.string :image
      t.text :description

      t.timestamps
    end
  end
end
