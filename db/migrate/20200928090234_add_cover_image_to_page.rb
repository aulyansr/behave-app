class AddCoverImageToPage < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :cover_image, :string
  end
end
