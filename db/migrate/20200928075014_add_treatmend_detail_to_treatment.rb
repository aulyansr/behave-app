class AddTreatmendDetailToTreatment < ActiveRecord::Migration[5.2]
  def change
    add_column :treatments, :treatment_detail_title, :string
    add_column :treatments, :treatment_detail_description, :text
    add_column :treatments, :slug, :string
  end
end
