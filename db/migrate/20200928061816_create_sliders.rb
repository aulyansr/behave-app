class CreateSliders < ActiveRecord::Migration[5.2]
  def change
    create_table :sliders do |t|
      t.string :image
      t.string :title
      t.string :page_id
      t.string :caption

      t.timestamps
    end
  end
end
