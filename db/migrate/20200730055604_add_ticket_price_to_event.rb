class AddTicketPriceToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :price, :string
  end
end
