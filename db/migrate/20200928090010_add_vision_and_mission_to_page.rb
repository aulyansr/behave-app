class AddVisionAndMissionToPage < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :vision_and_mission, :text
    add_column :pages, :our_story, :text
  end
end
