  $('.home-slider').owlCarousel({
    autoplayHoverPause:false,
    autoplayTimeout:6000,
    loop:true,
    dots: false,

    responsive:{
      0:{
        autoWidth:false,
        mouseDrag: false,
        center:false,
        items:1
      },
      600:{
        nav:true,
        items:3
      },
      1000:{
        autoWidth:true,
        center:true,
        nav:true,
        items:3
      }
    }

  })
  $(function () {
    $(document).scroll(function () {
      var $nav = $(".navbar");
      $nav.toggleClass('scrolled shadow', $(this).scrollTop() > $nav.height());
    });
  });
